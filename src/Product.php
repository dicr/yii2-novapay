<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 05.01.22 03:00:37
 */

declare(strict_types = 1);
namespace dicr\novapay;

use dicr\json\JsonEntity;

/**
 * Информация о товаре.
 */
class Product extends JsonEntity
{
    /** @var string|null payment position title */
    public ?string $description = null;

    /** @var int|null payment position count */
    public ?int $count = null;

    /** @var float|null payment position total price */
    public ?float $price = null;

    /**
     * @inheritDoc
     */
    public function attributeFields(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function rules() : array
    {
        return [
            ['description', 'trim'],
            ['description', 'required'],

            ['price', 'trim'],
            ['price', 'required'],
            ['price', 'number', 'min' => 0.01],
            ['price', 'filter', 'filter' => 'floatval'],

            ['count', 'trim'],
            ['count', 'required'],
            ['count', 'integer', 'min' => 1],
            ['count', 'filter', 'filter' => 'intval']
        ];
    }
}
