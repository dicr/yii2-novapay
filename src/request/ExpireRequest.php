<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 05.01.22 03:00:37
 */

declare(strict_types = 1);

namespace dicr\novapay\request;

use dicr\novapay\NovaPayRequest;

/**
 * Manually expire created session.
 */
class ExpireRequest extends NovaPayRequest
{
    /** @var string|null ID ранее созданной сессии */
    public ?string $sessionId = null;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            ['sessionId', 'trim'],
            ['sessionId', 'required']
        ];
    }

    /**
     * @inheritDoc
     */
    protected function func(): string
    {
        return 'expire';
    }
}
