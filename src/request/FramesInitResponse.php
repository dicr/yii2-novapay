<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 05.01.22 03:00:37
 */

declare(strict_types = 1);
namespace dicr\novapay\request;

use dicr\novapay\NovaPayResponse;

/**
 * Ответ FramesInit
 */
class FramesInitResponse extends NovaPayResponse
{
    /** @var string|null ID платежной сессии */
    public ?string $sessionId = null;

    /** @var string|null URL переадресации на платежную страницу */
    public ?string $url = null;

    /**
     * @inheritDoc
     */
    public function attributeFields(): array
    {
        return array_merge(parent::attributeFields(), [
            'sessionId' => 'session_id'
        ]);
    }
}
