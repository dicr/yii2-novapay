<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 05.01.22 03:00:37
 */

declare(strict_types = 1);

namespace dicr\novapay\request;

use dicr\novapay\NovaPay;
use dicr\novapay\NovaPayRequest;
use yii\base\Exception;
use yii\helpers\Json;

/**
 * Получить статус сессии.
 *
 * Return current session status.
 */
class GetStatusRequest extends NovaPayRequest implements NovaPay
{
    /** @var string|null payment session id */
    public ?string $sessionId = null;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            ['sessionId', 'trim'],
            ['sessionId', 'required']
        ];
    }

    /**
     * @inheritDoc
     */
    protected function func() : string
    {
        return 'get-status';
    }

    /**
     * Отправляет запрос.
     *
     * @return string статус сессии.
     * @throws Exception
     */
    public function send() : string
    {
        $data = parent::send();

        $status = (string)($data['status'] ?? '');
        if ($status === '') {
            throw new Exception('Не получен статус сессии: ' . Json::encode($data));
        }

        return $status;
    }
}
