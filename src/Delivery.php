<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 05.01.22 02:44:15
 */

declare(strict_types = 1);
namespace dicr\novapay;

use dicr\json\JsonEntity;

/**
 * Информация о доставке.
 */
class Delivery extends JsonEntity
{
    /** @var float|null объем * вес (minimum 0.01) */
    public ?float $volumeWeight = null;

    /** @var float|null вес (minimum 0.1) */
    public ?float $weight = null;

    /** @var ?string ID НоваПошта города получателя */
    public ?string $recipientCity = null;

    /** @var ?string ID отделения НоваПошта */
    public ?string $recipientWarehouse = null;

    /**
     * @inheritDoc
     */
    public function rules() : array
    {
        return [
            ['volumeWeight', 'number', 'min' => 0.01],
            ['volumeWeight', 'filter', 'filter' => 'floatval'],

            ['weight', 'number', 'min' => 0.1],
            ['weight', 'filter', 'filter' => 'floatval'],

            ['recipientCity', 'trim'],
            ['recipientCity', 'default'],

            ['recipientWarehouse', 'trim'],
            ['recipientWarehouse', 'default']
        ];
    }
}
