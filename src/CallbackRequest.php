<?php
/*
 * @copyright 2019-2022 Dicr http://dicr.org
 * @author Igor A Tarasov <develop@dicr.org>
 * @license MIT
 * @version 05.01.22 02:42:46
 */

declare(strict_types = 1);
namespace dicr\novapay;

use dicr\json\JsonEntity;

use function array_merge;

/**
 * Запрос от сервера NovaPay с уведомлением о статусе платежа.
 */
class CallbackRequest extends JsonEntity
{
    /** @var string|null платежная сессия */
    public ?string $sessionId = null;

    /** @var string|null статус сессии (STATUS_*) */
    public ?string $status = null;

    /** @var ?array метаданные, переданные во время создания сессии */
    public ?array $metadata = null;

    /** @var ?string имя */
    public ?string $firstName = null;

    /** @var ?string фамилия */
    public ?string $lastName = null;

    /** @var ?string отчество */
    public ?string $patronymic = null;

    /** @var ?string телефон */
    public ?string $phone = null;

    /** @var ?string номер заказа магазина */
    public ?string $externalId = null;

    /** @var ?Delivery информация о доставке */
    public ?Delivery $delivery = null;

    /** @var ?Product[] товары в чеке на оплату */
    public ?array $products = null;

    /** @var ?float стоимость доставки */
    public ?float $deliveryAmount = null;

    /**
     * @var ?int статус доставки
     * used if secure payment used. Statuses is taken from
     * @link https://devcenter.novaposhta.ua/docs/services/556eef34a0fe4f02049c664e/operations/55702cbba0fe4f0cf4fc53ee
     */
    public ?int $deliveryStatusCode = null;

    /** @var ?string статус доставки в текстовом виде */
    public ?string $deliveryStatusText = null;

    /**
     * @inheritDoc
     */
    public function attributeFields(): array
    {
        return array_merge(parent::attributeFields(), [
            'sessionId' => 'id',
            'firstName' => 'client_first_name',
            'lastName' => 'client_last_name',
            'patronymic' => 'client_patronymic',
            'phone' => 'client_phone'
        ]);
    }

    /**
     * @inheritDoc
     */
    public function attributeEntities(): array
    {
        return [
            'products' => [Product::class],
            'delivery' => Delivery::class
        ];
    }
}
